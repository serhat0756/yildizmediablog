@isset($categories)
<div class="col-md-3">
  <div class="card border-0">

    <div class="card-header font-weight-bold text-dark border">
     Kategoriler
   </div>

   <div class="list-group">

     @foreach ($categories as $category)

     <li class="list-group-item rounded-0">
      <a class="text-decoration-none @if(Request::segment(2)==$category->slug) text-info @endif" @if(Request::segment(2)!=$category->slug) href="{{route('category.detail',$category->slug)}}" @endif >{{$category->name}}</a>
      <span class="badge badge-danger float-right mt-1">{{$category->articleCount()}}</span>
    </li>
    @endforeach

  </div>

</div>

</div>
@endif