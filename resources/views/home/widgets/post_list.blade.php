<div class="col-md-9 mx-auto mb-3">
	@if(count($articles)>0)

	@foreach($articles as $article)
	<div class="post-preview">
		<a href="{{route('single' ,[$article->getCategory->slug,$article->slug])}}">
			<h2 class="post-title">
				{{$article->title}}
			</h2>
			<img width="800" height="400" src="{{$article->image}}">
			<h3 class="post-subtitle">
				{{Str::limit($article->content,100)}}
			</h3>
		</a>
		<p class="post-meta"> Kategori: {{$article->getCategory->name}}
			<span class="float-right">{{$article->created_at->diffForHumans()}}</span>
		</p>

	</div>

	@if(!$loop->last)
	<hr>
	@endif


	@endforeach


	{{$articles->links()}}

	@else

	<div class="alert alert-danger"><h5>Bu Kategoriye ait içerik bulunamadı..</h5></div>

	@endif
</div>