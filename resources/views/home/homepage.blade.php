@extends('home.layouts.master')
@section('title', 'Anasayfa Blogs')
@section('content')

@include('home.widgets.post_list')

@include('home.widgets.category_widgets')

@endsection
