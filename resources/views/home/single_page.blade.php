@extends('home.layouts.master')
@section('title', $article->title)
@section('bg', $article->image)
@section('content')

<div class="col-md-9 mx-auto mb-3">
  <div class="post-preview">
   <p>
     {!!$article->content!!}
      <div class="clearfix mb-2"></div>
      <span class="text-danger">Okunma: {{$article->hit}}</span>
   </p>
   
   <p class="post-meta"> Kategori: {{$article->getCategory->name}}
    <span class="float-right">{{$article->created_at->diffForHumans()}}</span>
  </p>
    
</div>

</div>

@include('home.widgets.category_widgets')

@endsection
