@extends('home.layouts.master')
@section('title', 'İletişim')
@section('bg', 'https://medtrainer.com/wp-content/uploads/2016/10/contact-bg.jpg')
@section('content')

<div class="col-md-8 mb-3">

	@if(session('success'))

	<div class="alert alert-success">
		{{session('success')}}
	</div>
	@endif

	@if($errors->any())

	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $message) 

			<li>{{$message}}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<p>Bizimle iletişime geçmek için aşağıdaki formu kullanabilirsiniz.</p>

	<form method="POST" action="{{route('contact.post')}}">
		@csrf
		<div class="form-group">
			<label for="name_surname">Ad Soyad</label>
			<input type="text" class="form-control" value="{{old('name_surname')}}" placeholder="Ad Soyad" id="name_surname" name="name_surname" required>
		</div>

		<div class="form-group">
			<label for="email">Mail</label>
			<input type="email" class="form-control" value="{{old('email')}}" placeholder="Mail" id="email" name="email" required>
		</div>

		<div class="form-group">
			<label for="phone">Telefon</label>
			<input type="tel" class="form-control" value="{{old('phone')}}" placeholder="Telefon" id="phone" name="phone">

		</div>


		<div class="form-group">
			<label for="subject" name="subject">Konu</label>
			<select class="form-control" id="subject" name="subject" required>
				<option value="genel" >Genel</option>
				<option value="şikayet">Şikayet</option>
				<option value="destek">Destek</option>


			</select>
		</div>

		<div class="form-group ">
			<label for="message">Mesajınız</label>
			<textarea rows="5" class="form-control" placeholder="Mesajınız" id="message" name="message" required>{{old('message')}}</textarea>
		</div>
		<br>
		<div id="success"></div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary" id="sendMessageButton">Gönder</button>
		</div>
	</form>

</div>
<div class="col-md-4">
	<div class="card p-2 shadow-sm rounded-0">
		<div class="mb-1"><b>Adres:</b>  adres mah sok apt il/ilçe</div>
		<div class="mb-1"><b>Telefon:</b>  +905328889465</div>
		<div class="mb-1"><b>Fax:</b>  +905328889465</div>
		<div class="mb-1"><b>Email:</b>  bilgi@bloginfo.com</div>
		
	</div>
</div>
@endsection