@extends('home.layouts.master')

@section('title',  $category->name)

@section('content')

@include('home.widgets.post_list')

@include('home.widgets.category_widgets')

@endsection
