@extends('home.layouts.master')
@section('title', $page->title)
@section('bg', $page->image)
@section('content')

<div class="col-md-9 mx-auto mb-3">
	<div class="post-preview">
		<p>
			{{$page->content}}
		</p>
	</div>
</div>
@endsection