<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	public function getCategory(){

		return $this->HasOne('App\Models\Category', 'id', 'category');
	}
}
