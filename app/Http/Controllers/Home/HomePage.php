<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

use App\Models\Category;
use App\Models\Article;
use App\Models\Page;
use App\Models\Contact;

class HomePage extends Controller {

	public function __construct(){

		view()->share('categories', Category::orderBy('id', 'DESC')->get());
		view()->share('pages', Page::orderBy('rank', 'ASC')->get());
	}

	public function index(){

		$data['articles']   = Article::orderBy('created_at', 'DESC')->paginate(2);
		return view('home.homepage', $data);
	}

	public function categoryDetail($slug){

		$category = Category::whereSlug($slug)->first() ?? abort(403, 'böyle bir kategori bulunamadı!');
		$data['category']   = $category;
		$data['articles']   = Article::whereCategory($category->id)->orderBy('created_at', 'DESC')->paginate(2);
		return view('home.category_detail', $data);
	}

	public function single($category,$slug){

		$category = Category::whereSlug($category)->first() ?? abort(403, 'böyle bir kategori bulunamadı!');
		$article = Article::whereSlug($slug)->whereCategory($category->id)->first() ?? abort(403, 'böyle bir yazı bulunamadı!');
		$article->increment('hit');
		$data['article']    = $article;
		return view('home.single_page', $data);
	}

	public function pageDetail($page){

		$data['page'] = Page::whereSlug($page)->first() ?? abort(403, 'böyle bir sayfa bulunamadı!');
		return view('home.page_detail', $data);
	}

	public function contact(){

		return view('home.contact');
	}

	public function contactform(Request $request){

		$rules =  [
			'name_surname'  => 'required|min:5',
			'email'         => 'required|email',
			'subject'       => 'required',
			'message'       => 'required|min:10'
		];


		$validator = Validator::make($request->all(),$rules);

		if ($validator->fails()) {
			return redirect()->route('contact')
			->withErrors($validator)
			->withInput();
		}

		$contact = new Contact;
		$contact->name_surname  = $request->name_surname;
		$contact->email         = $request->email;
		$contact->phone         = $request->phone;
		$contact->subject       = $request->subject;
		$contact->message       = $request->message;
		$contact->save();
		return redirect()->route('contact')->with('success', 'Mesajınız başarıyla gönderildi.');

	}


}
