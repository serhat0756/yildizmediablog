<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/panel', 'Dashboard\Panel@index')->name('admin.panel');

Route::get('/admin/login', 'Dashboard\AuthController@login')->name('admin.login');

Route::post('/admin/loginform', 'Dashboard\AuthController@logincheck')->name('login.form');








Route::get('/', 'Home\HomePage@index')->name('home');

Route::get('/kategori/{slug}', 'Home\HomePage@categoryDetail')->name('category.detail');

Route::get('/{category}/{slug}', 'Home\HomePage@single')->name('single');

Route::get('/iletisim', 'Home\HomePage@contact')->name('contact');

Route::post('/iletisim', 'Home\HomePage@contactform')->name('contact.post');

Route::get('/{page}', 'Home\HomePage@pageDetail')->name('page');

