<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
    		'name' => 'admin',
    		'email' => 'yildizmediabilisim@hotmail.com',
    		'password' =>  md5(123456),
    		'created_at' => now(),
    		'updated_at' => now()
    	]);
    }
}
