<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('admins')->insert([
    		'name' => 'admin',
    		'email' => 'yildizmediabilisim@hotmail.com',
    		'password' =>  bcrypt(12345678),
    		'image' => '',
    		'created_at' => now(),
    		'updated_at' => now()
    	]);
    }
}
