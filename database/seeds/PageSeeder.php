<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$pages = ['Hakkımızda', 'Kariyer', 'Vizyon', 'Misyon'];
    	$content = 'Sed rutrum arcu augue, ac lacinia orci pharetra non. Aliquam gravida in quam quis consectetur. Phasellus at scelerisque erat. Praesent porttitor nulla ac aliquam ornare. Ut porttitor ac massa eu imperdiet. Donec id lobortis leo, eget egestas neque. Fusce facilisis tristique leo et euismod. Aenean massa quam, lobortis et erat vitae, sagittis porttitor purus. Duis hendrerit consequat fringilla. Morbi a sem eget sapien commodo malesuada sed sed mauris. In eget metus risus. Integer arcu dolor, auctor in bibendum quis, porttitor ut odio. Morbi ante nibh, pharetra et massa at, consequat facilisis tortor. Vestibulum porta est sapien, vitae cursus tellus suscipit non.

    	Quisque maximus tristique pulvinar. Nulla et commodo lectus, ac efficitur nisl. Morbi iaculis lacus eu dui cursus tempor. Nullam at ultrices nisi. Nulla pellentesque lorem quis tellus dapibus, nec sollicitudin urna semper. Fusce in efficitur lacus. Curabitur condimentum, elit sit amet eleifend pulvinar, purus orci semper orci, interdum tincidunt nibh purus vitae mi. Donec ultrices lorem purus, eu molestie nisl ultricies sed.';

    	$image = 'https://www.webtekno.com/images/editor/default/0002/34/eb5b8a16abe376567cf1456663d9914027e427ba.jpeg';

    	$count = 0;

    	foreach ($pages as $key => $page) { 

    		$count++;

    		DB::table('pages')->insert([
    			'title'   => $page,
    			'content' => $content,
    			'image'   => $image,
    			'slug'   => Str::slug($page),
    			'rank'   => $count,
    			'created_at' => now(),
    			'updated_at' => now()
    		]);

    	}

    }
}
